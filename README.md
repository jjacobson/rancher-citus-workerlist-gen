# rancher-citus-workerlist-gen

`rancher-citus-workerlist-gen` is a service that uses `go-rancher-gen` and Rancher metadata to look for changes in worker service and update the worker list configuration used by the Citus master service.

The configuration file will contain the IPs of all containers with for the service named in the environment variable `SERVICE_NAME`.

The worker list file will be written to `pg_worker_list.conf` in the `/etc/citus` volume. After updating the file, `rancher-gen` will use `psql` to reinitialize the worker list on the Citus master.

## Usage

Add this service alongside the Citus master and worker services in your Rancher environment

### Environment Variables

NAME               | Required | Description
-------------------|----------|--------------------
POSTGRES_HOST      | Yes      | The postgres host to connect to.
POSTGRES_USER      | No       | The user for the postgres host. Defaults to `postgres`
POSTGRES_PASSWORD  | No       | The password for the postgres user. Defaults to empty.

See [`rancher-gen`](https://gitlab.com/jjacobson/rancher-gen) for more environment variables.

## License

MIT
