#!/bin/sh

set -e
PSQL_CMD="psql -h${POSTGRES_HOST} -U${POSTGRES_USER:-postgres}"
if [ ! -z "$POSTGRES_PASSWORD" ]; then
    PSQL_CMD="${PSQL_CMD} -p ${POSTGRES_PASSWORD}"
fi
export NOTIFY_CMD="${PSQL_CMD} -c 'SELECT master_initialize_node_metadata();'"

exec "$@"
