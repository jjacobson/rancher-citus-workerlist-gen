FROM registry.gitlab.com/jjacobson/rancher-gen:1.0.1

RUN apk --update add --no-cache postgresql-client

VOLUME /etc/citus

COPY docker-entrypoint.sh pg_worker_list.tmpl /

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["go-wrapper", "run"]
